import XCTest

import MidiSocketPackageTests

var tests = [XCTestCaseEntry]()
tests += MidiSocketPackageTests.allTests()
XCTMain(tests)
